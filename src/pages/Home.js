import React,{useState,useEffect} from 'react';
import { View,Text,FlatList,TouchableOpacity,Image} from 'react-native';
import axios from 'axios';
import {useDispatch,useSelector} from 'react-redux'
import { faker } from '@faker-js/faker';
import Icon from 'react-native-vector-icons/FontAwesome'
import { chat } from '../redux/chatSlice';


const baseURL = "https://tekichat.herokuapp.com/api"


const client =  axios.create({
    baseURL:baseURL,
})


export default function Home({navigation:{navigate}}){

    const dispatch = useDispatch()
    const [chats,setChats] = useState("")

    const userId = useSelector((state) => state.login.userLoginInfo.user.id)
    
    const getChat = async () => {
        try{
            const res = await client.get(`/chat/conversation/${userId}`)
            const _chat = res.data
            setChats(_chat)
        }catch(error){
            console.log(error);
        }
    }
    

    useEffect(()=>{
        getChat()
        const interval = setInterval(() => {
            getChat()
                console.log("reload home");
        }, 5000);

        return function cleanup() {
            console.log("clear Interval Home");
            clearInterval(interval)
        }
    },[]);

    const renderChat = (item) =>{
        return(
        <View style={{marginHorizontal:15,paddingBottom:15}}>
            <TouchableOpacity 
            style={{
                padding:7,
                backgroundColor:"white",
                width:300,
                height:70,
                borderRadius:25,
                shadowOffset: {
                    width: 5,
                    height: 2,
                },
                shadowOpacity: 0.3,
                shadowRadius: 500,
                elevation: 10,
            }} 
            onPress={()=>{
                const chatData = {
                    interlocutors:item.Sender
                }
                dispatch(chat(chatData))
                navigate("ChatRoom")
            }}
            > 
            <View style={{
                flexDirection:"row",
                paddingBottom:5,
                alignItems:"center",
                justifyContent:"space-between"
                }}>
                <View style={{flexDirection:"row",width:"80%"}}>
                    <Image style={{width:50,height:50,borderRadius:25}} source={{uri:faker.image.avatar()}}/>
                    <View style={{width:'100%'}}>
                        <View style={{flexDirection:'row',justifyContent:'space-between'}}>
                            <Text style={{fontWeight:"bold",paddingLeft:7,fontSize:20}}>
                                {item.Name}
                            </Text>
                            <TouchableOpacity onPress={()=>alert("This feature is coming soon!")}>
                                <Icon style={{marginTop:2,marginRight:7}} name="ellipsis-h" size={15} color="#42C2FF"/> 
                            </TouchableOpacity>
                        </View>
                        {/* <View style={{marginLeft:7,marginTop:2,marginBottom:2,width:"95%",height:2,borderColor:"#42C2FF",borderWidth:1}}/> */}
                        <View style={{flexDirection:'row'}}>
                                <Icon style={{marginTop:2,marginLeft:7}}
                                name="comment-o" size={15} color="#42C2FF"/> 
                                <Text style={{paddingLeft:7}}>
                                    {item.Message}
                                </Text>
                        </View>
                    </View>
                </View>
            </View>

            </TouchableOpacity>
        </View>
        )
    }

    return(
        <View style={{flex:1,justifyContent:"space-between"}}>
            <View style={{flex:1,alignItems:"center",justifyContent:"center",marginTop:13}}>
            <FlatList
                data={chats}
                keyExtractor={(item,index) => `${item._id}-${index}`}
                renderItem={({item}) =>{
                    return(
                        renderChat(item)
                    );
                }}
            />
            </View>
            <TouchableOpacity style={{
                marginBottom:40,
                marginLeft:"80%",
                width:70,
                height:50,
                borderRadius:15,
                backgroundColor:"#42C2FF",
                justifyContent:"center",
                alignItems:"center",
            }}
            onPress={()=>navigate("NewChat")}
            >
                <Icon name="comment-o" size={35} color="white"/> 
                <Text style={{color:"white",fontSize:10}}>New Chat</Text>
            </TouchableOpacity>
        </View>
    )
};

