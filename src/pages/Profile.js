import React from 'react';
import { View,Text,Button,TouchableOpacity,Image} from 'react-native';
import {useDispatch,useSelector} from 'react-redux'
import { faker } from '@faker-js/faker'


export default function Profile({navigation:{navigate}}) {
    const username = useSelector((state) => state.login.userLoginInfo.user.name)
    const phone = useSelector((state) => state.login.userLoginInfo.user.phone)
    const email = useSelector((state) => state.login.userLoginInfo.user.email)
    const address = useSelector((state) => state.login.userLoginInfo.user.address)

    return(
        <View style={{flex:1,justifyContent:"space-around"}}>
            <View style={{alignItems:"center",height:"80%"}}>
                <View style={{flexDirection:"row",width:"90%",height:"35%",justifyContent:"space-between"}}>
                    <View style={{
                        width:"40%",
                        height:"100%",
                        backgroundColor:"white",
                        borderRadius:25,
                        justifyContent:"center",
                        alignItems:"center",
                        shadowOffset: {
                            width: 5,
                            height: 2,
                        },
                        shadowOpacity: 0.3,
                        shadowRadius: 500,
                        elevation: 5,
                    }}>
                            {/* <Text>
                                Photo
                            </Text> */}
                            <Image style={{width:'97%',height:'97%',borderRadius:25}} source={{uri:faker.image.avatar()}}/>
                    </View>
                    <View style={{
                        width:"55%",
                        height:"100%",
                        flexDirection:"row",
                        justifyContent:"space-around",
                        alignItems:"center",
                        backgroundColor:'white',
                        borderRadius:25,
                        padding:5,
                        shadowOffset: {
                            width: 5,
                            height: 2,
                        },
                        shadowOpacity: 0.3,
                        shadowRadius: 500,
                        elevation: 5,
                    }}>
                        <View>
                            <Text style={{marginBottom:7}}>Phone</Text>
                            <Text style={{marginBottom:7}}>E-mail</Text>
                            <Text style={{marginBottom:7}}>Address</Text>
                        </View>
                        <View style={{marginLeft:15}}>
                            <Text style={{marginBottom:7}}>:  {phone}</Text>
                            <Text style={{marginBottom:7}}>:  {email}</Text>
                            <Text style={{marginBottom:7}}>:  {address}</Text>
                        </View>
                    </View>
                </View>
                <View style={{
                    height:"30%",width:"90%",marginTop:15,flexDirection:"row",
                    backgroundColor:'white',borderRadius:25,justifyContent:'space-around',
                    shadowOffset: {
                        width: 5,
                        height: 2,
                    },
                    shadowOpacity: 0.3,
                    shadowRadius: 500,
                    elevation: 5,
                    }}>
                    <View style={{flexDirection:"row",marginTop:'5%'}}>
                        <View>
                            <Text style={{marginBottom:7}}>Gender</Text>
                            <Text style={{marginBottom:7}}>Hobby</Text>
                            <Text style={{marginBottom:7}}>Skill</Text>
                        </View>
                        <View style={{marginLeft:15}}>
                            <Text style={{marginBottom:7}}>:  Not set</Text>
                            <Text style={{marginBottom:7}}>:  Not set</Text>
                            <Text style={{marginBottom:7}}>:  Not set</Text>
                        </View>
                    </View>
                    <View style={{flexDirection:"row",marginTop:'5%',marginLeft:"17%"}}>
                        <View>
                            <Text style={{marginBottom:7}}>Website</Text>
                            <Text style={{marginBottom:7}}>Occupancy</Text>
                            <Text style={{marginBottom:7}}>Skill</Text>
                        </View>
                        <View style={{marginLeft:15}}>
                            <Text style={{marginBottom:7}}>:  Not set</Text>
                            <Text style={{marginBottom:7}}>:  Not set</Text>
                            <Text style={{marginBottom:7}}>:  Not set</Text>
                        </View>
                    </View>
                </View>
                
                <TouchableOpacity onPress={()=>alert("This feature is coming soon!")}
                style={{
                    width:"25%",
                    height:35,
                    backgroundColor:"#F9A03F",
                    borderRadius:25,
                    alignItems:"center",
                    justifyContent:"center",
                    shadowOffset: {
                        width: 5,
                        height: 2,
                    },
                    shadowOpacity: 0.3,
                    shadowRadius: 500,
                    elevation: 5,
                    marginTop:10
                }}
                >
                    <Text style={{color:"white"}}>Edit Profile</Text>    
                </TouchableOpacity>
            </View>
            
            <View style={{height:"10%",alignItems:"center"}}> 
                <TouchableOpacity onPress={()=>navigate('Login')}
                style={{
                    width:"25%",
                    height:"50%",
                    backgroundColor:"#DE3C4B",
                    borderRadius:25,
                    alignItems:"center",
                    justifyContent:"center",
                    shadowOffset: {
                        width: 5,
                        height: 2,
                    },
                    shadowOpacity: 0.3,
                    shadowRadius: 500,
                    elevation: 5,
                }}
                >
                    <Text style={{color:"white"}}>Log Out</Text>    
                </TouchableOpacity>
            </View>
        </View>
            
    )
};