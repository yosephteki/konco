import axios from 'axios';
import React,{useState,useEffect} from 'react';
import { View, Text,FlatList,TouchableOpacity,Image} from 'react-native';
import {useSelector} from 'react-redux'
import { faker } from '@faker-js/faker'
import {useDispatch} from 'react-redux'
import { chat } from '../redux/chatSlice';
import Icon from 'react-native-vector-icons/FontAwesome'


export default function NewChat({navigation:{navigate}}){
    const baseURL = "https://fellas.herokuapp.com/user-api"
    const token = useSelector((state) => state.login.userLoginInfo.token)

    const dispatch = useDispatch()

    
    const client = axios.create({
        baseURL:baseURL,
    })
    const [interlocutors,setInterlocutors] = useState("")
    
    useEffect(()=>{
        getInterlocutors()
    },[])

    const getInterlocutors = async () => {
        try {
            await client.get(`/users`,{headers: {'Authorization': `Basic ${token}`}}).
            then((res => setInterlocutors(res.data)))
        } catch (error) {
            console.log(error);
        }
    }
    
    const renderInterlocutors = (item) =>{
        return(
        <View style={{marginHorizontal:15,paddingBottom:15,flexDirection:'row',justifyContent:'space-between',alignItems:'center'}}>
            <TouchableOpacity 
            style={{
                padding:7,
                backgroundColor:"white",
                width:"50%",
                height:65,
                borderRadius:25,
                shadowOffset: {
                    width: 5,
                    height: 2,
                },
                shadowOpacity: 0.3,
                shadowRadius: 500,
                elevation: 10,
            }} 
            onPress={()=>{
                const chatData = {
                    interlocutors:item.id
                }
                dispatch(chat(chatData))
                navigate("ChatRoom")
            }}
            > 
            <View style={{
                flexDirection:"row",
                paddingBottom:5,
                alignItems:"center",
                justifyContent:'space-between'
                }}>
                <View style={{flexDirection:'row'}}>
                    <Image style={{width:50,height:50,borderRadius:25}} source={{uri:faker.image.avatar()}}/>
                    <View>
                        <Text style={{fontWeight:"bold",paddingLeft:7,fontSize:20}}>
                            {item.name}
                        </Text>
                        <View style={{flexDirection:'row',alignItems:'center',paddingLeft:7}}>
                            <View style={{width:10,height:10,borderRadius:5,marginRight:3,backgroundColor:"green"}}/>
                            <Text style={{fontSize:10}}>
                                Online
                            </Text>
                        </View>
                    </View>
                </View>
            </View>
            </TouchableOpacity>
            <View style={{flexDirection:'row',width:'47%',justifyContent:'space-around'}}>
                <TouchableOpacity style={{width:50,height:50,backgroundColor:'white',borderRadius:25,justifyContent:'center',alignItems:'center',shadowOffset: {width: 5,height: 2,},shadowOpacity: 0.3,shadowRadius: 500,elevation: 10,}}
                onPress={()=>alert("This feature is coming soon!")}
                >
                    <Icon name="phone" size={25} color="#42C2FF"/> 
                </TouchableOpacity>
                <TouchableOpacity style={{width:50,height:50,backgroundColor:'white',borderRadius:25,justifyContent:'center',alignItems:'center',shadowOffset: {width: 5,height: 2,},shadowOpacity: 0.3,shadowRadius: 500,elevation: 10,}}
                 onPress={()=>{
                    const chatData = {
                        interlocutors:item.id
                    }
                    dispatch(chat(chatData))
                    navigate("ChatRoom")
                }}
                >
                    <Icon name="comments" size={25} color="#42C2FF"/> 
                </TouchableOpacity>
                <TouchableOpacity style={{width:50,height:50,backgroundColor:'white',borderRadius:25,justifyContent:'center',alignItems:'center',shadowOffset: {width: 5,height: 2,},shadowOpacity: 0.3,shadowRadius: 500,elevation: 10,}}
                onPress={()=>alert("This feature is coming soon!")}
                >
                    <Icon name="ellipsis-v" size={25} color="#42C2FF"/> 
                </TouchableOpacity>
            </View>
        </View>
        )
    }

    return (
        <View>
            <FlatList
                style={{marginTop:15}}
                data={interlocutors}
                keyExtractor={(item,index) => `${item._id}-${index}`}
                renderItem={({item}) =>{
                    return(
                            renderInterlocutors(item)
                    );
                }}
            />
        </View>
    );
};


