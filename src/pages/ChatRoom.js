import React,{useState,useEffect} from 'react';
import { View,Text,FlatList,TouchableOpacity,StyleSheet,TextInput, Button} from 'react-native';
import {useDispatch,useSelector} from 'react-redux'
import axios from 'axios';

const baseURL = "https://tekichat.herokuapp.com/api/chat/"

const client =  axios.create({
    baseURL:baseURL,
})

export default function ChatRoom({navigation:{navigate}}){
    const interlocutors = useSelector((state) => state.chat.chat.interlocutors)
    const userId = useSelector((state) => state.login.userLoginInfo.user.id)


    const [chat,setChat] = useState([])
    const [message, setMessage] = useState("");


    const getChat = async () => {
        try{
            const res = await client.get(`conversation/chatroom/${userId}/${interlocutors}`).
            then((res => setChat(res.data)))
        }catch(error){
            console.log(error);
        }
    }

    
    const sendMessage = async () => {
        try{
            const res = await client.post(`${userId}/${interlocutors}/${message}`)
            console.log("res",res);
            setMessage("")
            getChat()
        }catch(error){
            console.log(error);
        }
    }

    useEffect(()=>{
        getChat()
        const interval = setInterval(() => {
            getChat()
                console.log("reload chatroom");
        }, 3000);

        return function cleanup() {
            console.log("Clear interval chatroom");
            clearInterval(interval)
        }
    },[]);

    const renderChat = (item) =>{
        return(
            <TouchableOpacity 
            style={{
                backgroundColor:'white',
                marginBottom:10,
                borderRadius:25,
                shadowOffset: {
                    width: 5,
                    height: 2,
                },
                shadowOpacity: 0.3,
                shadowRadius: 500,
                elevation: 5,
            }} 
            > 
                <Text style={{padding:15}}>
                    {item.Message}
                </Text>
            </TouchableOpacity>
        )
    }

    return(
        <View style={{flex:1,justifyContent:'space-between'}}>
            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
            <FlatList
                inverted
                style={{width:'100%'}}
                data={chat.Conversation}
                keyExtractor={(item,index) => `${item._id}-${index}`}
                renderItem={({item}) =>{
                    return(
                        <View style={[styles.message, item.Senderid == userId ? styles.user : styles.other]}>
                            {renderChat(item)}
                        </View>
                    );
                }}
            />
            </View>
            <View style={{flexDirection:'row',justifyContent:"space-around",alignItems:"center",marginBottom:10}}>
                <TextInput
                style={{
                    borderWidth: 1,
                    paddingVertical: 10,
                    borderRadius: 5,
                    paddingHorizontal: 10,
                    width:'80%'
                }}
                placeholder="Enter Your Message"
                value={message}
                onChangeText={(value)=>setMessage(value)}
                />
                <TouchableOpacity 
                    style={{
                        backgroundColor:'#42C2FF',
                        width:'15%',
                        height:50,
                        justifyContent:'center',
                        alignItems:'center',
                        borderRadius:15
                        }}
                    onPress={sendMessage}
                >
                    <Text style={{color:'white'}}>
                        Send
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    message: {
    },
    user: {
        alignSelf:'flex-end',
        marginRight:7
    },
    other: {
        
        alignSelf:'flex-start',
        marginLeft:7
    },
});