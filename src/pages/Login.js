import React,{useState} from 'react'
import { View,Text,StyleSheet,TextInput,Button,TouchableOpacity } from 'react-native'
import {useDispatch} from 'react-redux'
import {login} from '../redux/loginSlice'
import axios from 'axios'

const baseURL = "https://fellas.herokuapp.com/user-api"

const fellas =  axios.create({
  baseURL:baseURL,
})

export default function Login({navigation : {navigate}}){
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [token,setToken] = useState("");

    const dispatch = useDispatch()

    const userLogin = async () => {
      const loginData = {
        email:username,
        password:password
      }
      try{
          const res = await fellas.post(`/userLogin`, loginData)
          const userLoginData = {
            user : res.data.user,
            token : res.data.token
          }
          dispatch(login(userLoginData))
          setUsername("")
          setPassword("")
          console.log(res.data);
          setToken(res.data)
          navigate("HomeScreen")
      }catch(error){
        alert("FAIL TO LOGIN")
        console.log(error)
      }
  }
    const userRegister = () => {
      navigate("Register")
    }

    return (
        <View style={styles.container}>
          <View style={{justifyContent:"center",alignItems:"center"}}>
            <View 
            style={{
              width:150,
              height:150,
              borderRadius:75,
              backgroundColor:"#42C2FF",
              justifyContent:"center",
              alignItems:"center",
              marginBottom:30,
              }}>
              <Text style={{color:"white",fontSize:40}}>
                KONCO
              </Text>
            </View>
            <TextInput
              style={{
                borderWidth: 1,
                paddingVertical: 10,
                borderRadius: 5,
                width: 300,
                marginBottom: 10,
                paddingHorizontal: 10,
              }}
              placeholder="Input Email"
              value={username}
              onChangeText={(value)=>setUsername(value)}
            />
            <TextInput
              style={{
                borderWidth: 1,
                paddingVertical: 10,
                borderRadius: 5,
                width: 300,
                marginBottom: 10,
                paddingHorizontal: 10,
              }}
              placeholder="Input Password"
              secureTextEntry
              value={password}
              onChangeText={(value)=>setPassword(value)}
            />
            <TouchableOpacity 
            onPress={userLogin}
            style={{
              backgroundColor:"#42C2FF",
              width:200,
              height:35,
              borderRadius:25,
              justifyContent:"center",
              alignItems:"center"
          }}
            >
              <Text style={{color:"white"}}>
                Log In  
              </Text>  
            </TouchableOpacity>
            <TouchableOpacity onPress={userRegister} style={{
              backgroundColor:"#A0CA92",
              width:200,
              height:35,
              borderRadius:25,
              justifyContent:"center",
              alignItems:"center",
              marginTop:15
            }}
            >
              <Text style={{color:"white"}}>
                Register
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      );
};

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: "white",
      justifyContent: "center",
      alignItems: "center",
    },
  });