import React,{useState} from 'react'
import { View,Text,StyleSheet,TextInput,Button,TouchableOpacity,KeyboardAvoidingView} from 'react-native'
import axios from 'axios'

const fellas = "https://fellas.herokuapp.com/user-api"

const client =  axios.create({
  baseURL:fellas,
})

export default function Register({navigation:{navigate}}) {
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [phone, setPhone] = useState("");
    const [address, setAddress] = useState("");
    const [password, setPassword] = useState("");


    const resetForm = () => {
        setAddress("")
        setEmail("")
        setPassword("")
        setPhone("")
        setUsername("")
    }

    const userRegister = async () => {
        try {
            const res = await client.post(`/user`,{
                name:username,
                email:email,
                phone:phone,
                address:address,
                password:password
            })
            resetForm()
            alert("Successfully create new account,please login")
            navigate("Login")
        } catch (error) {
            console.log(error);
            alert("Failed to create account")
        }

    }

    // const sendMessage = async () => {
    //     try{
    //         const res = await client.post(`${userId}/${interlocutors}/${message}`)
    //         console.log("res",res);
    //         setMessage("")
    //         getChat()
    //     }catch(error){
    //         console.log(error);
    //     }
    // }

    return(
        <KeyboardAvoidingView style={{flex:1,alignItems:"center",justifyContent:'center'}}>
          <View style={{width:'90%',height:'75%',backgroundColor:'white',
            shadowOffset: {
              width: 5,
              height: 2,
              },
            shadowOpacity: 0.3,
            shadowRadius: 500,
            elevation: 5,
            justifyContent:'space-around',
            alignItems:'center',
            borderRadius:25
            }}>
            <Text style={{fontSize:25}}>Create Account</Text>
            <View>
              <TextInput
                  style={{
                    borderWidth: 1,
                    paddingVertical: 10,
                    borderRadius: 5,
                    width: 300,
                    marginBottom: 10,
                    paddingHorizontal: 10,
                  }}
                  placeholder="Input Your Username"
                  value={username}
                  onChangeText={(value)=>setUsername(value)}
                  />
              <TextInput
                  style={{
                    borderWidth: 1,
                    paddingVertical: 10,
                    borderRadius: 5,
                    width: 300,
                    marginBottom: 10,
                    paddingHorizontal: 10,
                  }}
                  placeholder="Input Your E-mail"
                  value={email}
                  onChangeText={(value)=>setEmail(value)}
                  />
              <TextInput
                keyboardType='number-pad'
                style={{
                  borderWidth: 1,
                  paddingVertical: 10,
                  borderRadius: 5,
                  width: 300,
                  marginBottom: 10,
                  paddingHorizontal: 10,
                  }}
                  placeholder="Input Your Phone Number"
                  value={phone}
                  onChangeText={(value)=>setPhone(value)}
                  />
              <TextInput
                  style={{
                    borderWidth: 1,
                    paddingVertical: 10,
                    borderRadius: 5,
                    width: 300,
                    marginBottom: 10,
                    paddingHorizontal: 10,
                  }}
                  placeholder="Input Your Address"
                  value={address}
                  onChangeText={(value)=>setAddress(value)}
                  />
              <TextInput
                  style={{
                    borderWidth: 1,
                    paddingVertical: 10,
                    borderRadius: 5,
                    width: 300,
                    marginBottom: 10,
                    paddingHorizontal: 10,
                  }}
                  placeholder="Input Your Password"
                  value={password}
                  secureTextEntry
                  onChangeText={(value)=>setPassword(value)}
                  />
            </View>
            <View style={{flexDirection:'row',justifyContent:'space-around',width:'100%'}}>
              <TouchableOpacity onPress={()=>navigate("Login")} style={{backgroundColor:'#DE3C4B',
              width:100,height:40,borderRadius:25,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'white'}}>
                Cancel
                </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={userRegister} style={{backgroundColor:'#42C2FF',
              width:100,height:40,borderRadius:25,justifyContent:'center',alignItems:'center'}}>
                <Text style={{color:'white'}}>
                Register
                </Text>
                </TouchableOpacity>
            </View>
          </View>
        </KeyboardAvoidingView>
    )
};