import React from 'react';
import { View,Text, TouchableOpacity,Image } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import {useDispatch,useSelector} from 'react-redux'
import { faker } from '@faker-js/faker';


export default function HomeHeader() {
    const navigation = useNavigation()
    const username = useSelector((state) => state.login.userLoginInfo.user.name)


    return(
        <View style={{backgroundColor:'#42C2FF',width:"80%",height:"100%",shadowOffset: {
            width: 5,
            height: 2,
        },
        shadowOpacity: 0.3,
        shadowRadius: 500,
        elevation: 10,
        flexDirection:"row",
        justifyContent:"space-between",
        alignItems:'center'
        }}>
            <View style={{marginLeft:15}}>
                <Text style={{color:"white",fontSize:20}}>
                    Konco
                </Text>
            </View>
            <TouchableOpacity style={{justifyContent:"center",alignItems:"center",marginRight:'5.5%'}} onPress={()=>navigation.navigate("Profile")}>
                <Image style={{width:35,height:35,borderRadius:15}} source={{uri:faker.image.avatar()}}/>
                {/* <Text style={{color:"white",fontWeight:"bold"}}>
                    {username}
                </Text> */}
            </TouchableOpacity>
        </View>
    )
};