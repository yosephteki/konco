import { configureStore } from "@reduxjs/toolkit";
import {chatSlice} from "./chatSlice";
import { loginSlice } from "./loginSlice";

export const store = configureStore({
  reducer: {
  [loginSlice.name]: loginSlice.reducer,
  [chatSlice.name]: chatSlice.reducer
  },
});