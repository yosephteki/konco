import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    chat : {
        interlocutors : ""
    }
};

export const chatSlice = createSlice({
    name: "chat",
    initialState,
    reducers: {
        chat: (state, action) => {
            state.chat.interlocutors = action.payload.interlocutors;
        },
    },
});

export const { chat } = chatSlice.actions;

export default chatSlice.reducer;