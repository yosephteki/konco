import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    userLoginInfo : {
        user : {},
        token : ""
    }
};

export const loginSlice = createSlice({
    name: "login",
    initialState,
    reducers: {
        login: (state, action) => {
            state.userLoginInfo.user = action.payload.user;
            state.userLoginInfo.token = action.payload.token;
        },
    },
});

export const { login } = loginSlice.actions;

export default loginSlice.reducer;