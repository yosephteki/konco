import React from 'react';
import 'react-native-gesture-handler'
import Icon from 'react-native-vector-icons/FontAwesome'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

import Home from './pages/Home'
import Login from './pages/Login'
import NewChat from './pages/NewChat';
import Profile from './pages/Profile';
import Register from './pages/Register';
import ChatRoom from './pages/ChatRoom';
import HomeHeader from './components/HomeHeader';


const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

const HomeScreen = () => {
    return(
        <Tab.Navigator
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;
            if (route.name === 'Home') {
              iconName = focused
                ? 'home' : 'home';
            } else if (route.name === 'Profile') {
              iconName = focused ? 'user' : 'user';
            }
            return <Icon name={iconName} size={size} color={color} />;
          },
          tabBarActiveTintColor: '#42C2FF',
          tabBarInactiveTintColor: 'gray',
        })}
      >
            <Tab.Screen name="Home" component={Home} options={{headerShown:false}}/>
            <Tab.Screen name="Profile" component={Profile} options={{headerShown:false}}/>
        </Tab.Navigator>
    )
}


export default function index() {
    return(
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Login" component={Login} options={{headerShown:false}}/>
                <Stack.Screen name="HomeScreen" component={HomeScreen} 
                options={{ headerLeft: () => <HomeHeader/>,title:null}}/>
                <Stack.Screen name="ChatRoom" component={ChatRoom}/>
                <Stack.Screen name="Register" component={Register} options={{headerShown:false}}/>
                <Stack.Screen name="NewChat" component={NewChat}/>
            </Stack.Navigator>
        </NavigationContainer>
    )
};

