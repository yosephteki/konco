import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Index from './src/index'
import {store} from './src/redux/store'
import {Provider} from 'react-redux'
import {decode, encode} from 'base-64'

if (!global.btoa) {  global.btoa = encode }

if (!global.atob) { global.atob = decode } 

export default function App() {
  return (
    <Provider store={store}>
      <Index/>
    </Provider>
  );
}
